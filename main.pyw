#!/usr/bin/python3
# -*- coding: utf-8 -*-

# автор: MrBoriska
# лицензия: свободная


import os, sys, math, re
from PyQt5 import QtCore, QtGui, QtWidgets, uic

# абсолютный путь до программы
def m_p(path):
    if hasattr(sys, "frozen"):
        return os.path.dirname(
            sys.executable
        )+'/'+path
    return os.path.dirname(os.path.realpath('__file__'))+'/'+path


# Конвертирование отдельного члена выражения в число
def item_to_float(x):
    if not type(x) is str:
        return x

    # Число Пи
    elif x == 'p':
        return math.pi
    # Число Эйлера
    elif x == 'e':
        return math.e

    # Член выражения задан факториалом
    elif x[-1] == '!':
        return doing('fac',calc(x[:-1]))

    # Используется упрошенная запись(без знака умножения) для символьных констант
    elif x[0] == 'p':
        return math.pi * item_to_float(calc(x[1:]))
    elif x[0] == 'e':
        return math.e * item_to_float(calc(x[1:]))
    elif x[-1] == 'p':
        return math.pi * item_to_float(calc(x[:-1]))
    elif x[-1] == 'e':
        return math.e * item_to_float(calc(x[:-1]))

    # Задан функцией
    elif x.startswith("sin("):
        return doing('sin',calc(x[4:-1]))
    elif x.startswith("cos("):
        return doing('cos',calc(x[4:-1]))
    elif x.startswith("tan("):
        return doing('tan',calc(x[4:-1]))
    elif x.startswith("log["):
        a = calc(x[4:x.index("](")])
        b = calc(x[x.index("](")+2:-1])
        return doing('log',a,b)
    else:
        return float(x)

# Арифметические операции
def doing(opr,a,b=False): # вызывает нужные функции для операторов
    
    a = item_to_float(a)
    if b != False: 
        b = item_to_float(b)
    
    if opr == '+': return a + b
    elif opr == '-': return a - b
    elif opr == '*': return a * b
    elif opr == '/': return a / b
    elif opr == '^': return a ** b
    elif opr == 'fac':
        return math.factorial(a)
    elif opr == 'log':
        return math.log(b,a)
    elif opr == 'sin':
        if a > 360: a = a % 360
        return math.sin(math.radians(a))
    elif opr == 'cos':
        if a > 360: a = a % 360
        return math.cos(math.radians(a))
    elif opr == 'tan':
        if a > 360: a = a % 360
        return math.tan(math.radians(a))
    
    return False

# управляет в каком порядке производить команды
ords = ['^','/','*','-','+']

def calc(text):
    
    # Если выражение начинается с отрицательного числа,
    # то добавляем в начало ноль для формир.струк.
    if text[0] in ords[3:]: text = '0' + text
    
    # Формируем структуру выражения
    # oprs - список операторов
    # и ints - список чисел, синхронизированных позиционно с oprs
    oprs = []
    chi = ''
    ints = []
    s = 0
    for x in text:
        # Пропускаем то, что внутри скобок
        if x == '(': s += 1
        if s and x == ')': s -= 1
        # Если встречаем знак такой, что он не внутри скобки и не образует конструкцию типа 'XXe+YY' 'XXe-YY'
        if not s and x in ords and not (chi[-1] == 'e' and x in ['+','-']):
            oprs.append(x)

            # если элемент это выражение внутри скобок считаем его отдельно
            if chi[0] == '(' and chi[-1] == ')':
                chi = str(calc(chi[1:-1]))

            ints.append(chi)
            chi = ''
        else:
            chi += x
    # Действия для последего элемента, такие же как и для всех.
    if chi[0] == '(' and chi[-1] == ')':
        chi = str(calc(chi[1:-1]))
    ints.append(chi)
    
    # Выполняем команды, записывая результат в ints(собственно сердце калькулятора)
    d = 0
    pord = len(oprs)+1
    for x in ords:
        for i, y in enumerate(oprs):
            if x == y:
                d += 1
                if pord > i: d -= 1
                ints[i-d] = doing(x, ints[i-d], ints.pop(i-d+1))
                pord = i
    
    return ints[0]

# GUI
class MyWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(MyWindow, self).__init__()
        uic.loadUi(m_p('calc.ui'), self)
         # Вызывает функцию inputkeys() при событии clicked у элементов не ручного ввода
        self.plus.clicked.connect(lambda: self.inputkeys('+',1))
        self.minus.clicked.connect(lambda: self.inputkeys('-',1))
        self.set.clicked.connect(lambda: self.inputkeys('*',1))
        self.stepen.clicked.connect(lambda: self.inputkeys('^',1))
        self.div.clicked.connect(lambda: self.inputkeys('/',1))
        self.lckob.clicked.connect(lambda: self.inputkeys('('))
        self.rckob.clicked.connect(lambda: self.inputkeys(')'))
        self.int_float.clicked.connect(lambda: self.inputkeys('.',1))
        self.int_0.clicked.connect(lambda: self.inputkeys('0'))
        self.int_1.clicked.connect(lambda: self.inputkeys('1'))
        self.int_2.clicked.connect(lambda: self.inputkeys('2'))
        self.int_3.clicked.connect(lambda: self.inputkeys('3'))
        self.int_4.clicked.connect(lambda: self.inputkeys('4'))
        self.int_5.clicked.connect(lambda: self.inputkeys('5'))
        self.int_6.clicked.connect(lambda: self.inputkeys('6'))
        self.int_7.clicked.connect(lambda: self.inputkeys('7'))
        self.int_8.clicked.connect(lambda: self.inputkeys('8'))
        self.int_9.clicked.connect(lambda: self.inputkeys('9'))
        self.logarifm.clicked.connect(lambda: self.inputkeys('log[2]('))
        self.sinus.clicked.connect(lambda: self.inputkeys('sin('))
        self.cosinus.clicked.connect(lambda: self.inputkeys('cos('))
        self.tangens.clicked.connect(lambda: self.inputkeys('tan('))
        self.factorial.clicked.connect(lambda: self.inputkeys('!'))
        self.piconst.clicked.connect(lambda: self.inputkeys('π'))
        self.econst.clicked.connect(lambda: self.inputkeys('e'))
        self.clear.clicked.connect(lambda: self.textEdit.setText(''))
        self.clearEnd.clicked.connect(lambda: self.textEdit.backspace())

        # Вызываем калькулятор при событии clicked у элемента "равно"
        self.result.clicked.connect(lambda: self.output())
        
        self.show()
    
    def inputkeys(self, key, opr = 0):
        # Получение текста из textEdit
        text = self.textEdit.text()
        
        if (text and text[-1] != key) or opr == 0:
            # назначение текста в textEdit
            self.textEdit.insert(key)

            # Автозакрывание скобок и перемещение курсора внутрь скобки
            if key[-1] == '(':
                pt = self.textEdit.cursorPosition()
                
                if pt > len(text):
                    self.textEdit.insert(')')
                    self.textEdit.setCursorPosition(pt)
    
    def output(self):
        # Заменяем красивое число π, на некрасивое p, 
        # ибо третья версия питона не умеет нормально работать с юникодом.
        text = self.textEdit.text().replace('π','p')
        
        if text or text == 0:
            log = open(m_p('calc.log'), 'a', encoding='utf-8')
            log.write(text + '=')
        
        try:
            text = str(round(item_to_float(calc(text)), 15))
        except Exception as e: 
            text = str(e)
        
        self.textEdit.setText(text.replace("inf","∞"))
        
        if text or text == 0:
            log.write(text + "\n")
            log.close()

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    app.setStyle("fusion")
    window = MyWindow()
    sys.exit(app.exec_())
